#!/bin/sh
# Paul Sladen, 17 June 2016
# Included at recommendation of Mark Taylor (upstream); based on information in:
# http://www.star.bristol.ac.uk/~mbt/jsamp/commands.html
# https://github.com/Starlink/starjava/blob/master/jsamp/src/script/jsamp
# The Java class already includes command-line/help handling.

if [ $# = 0 ]; then
    JSAMP_SCRIPT_ARGS=-help
else
    JSAMP_SCRIPT_ARGS="$@"
fi

exec /usr/bin/java -classpath /usr/share/java/jsamp.jar org.astrogrid.samp.JSamp $JSAMP_SCRIPT_ARGS
